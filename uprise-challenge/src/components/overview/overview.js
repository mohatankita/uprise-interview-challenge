import React, { Component } from 'react';
import classes from './overview.module.css';
import { Button } from '@uprise/button';
import { connect } from 'react-redux';
import { albumDetail } from '../../store/actions/dispatcher';
import Loader from '../UI/loader/loader';

class Overview extends Component {
    async componentDidMount() {
        await this.props.getdetails();
    }
    render() {
        let details = (
            <div className="row view">
                    <div className="col-5 viewImage">
                        <div className={classes.imageBox}>
                            <img src={this.props.image} alt={this.props.albumName} />
                        </div>
                    </div>
                    <div className="col-7 viewImage">
                        <div className="viewDetail">
                            <h4><b>{this.props.albumName}</b></h4>
                            <br />
                            Followers ({this.props.followers})
                            </div>
                            <div className={classes.follow}>
                                <Button title="Follow" size="small" />
                            </div>
                    </div>
                </div>
            );

        if( this.props.loading ) {
            details = <Loader />
        }

        let errorMessage = null;
        if( this.props.error ) {
            errorMessage = <p> { this.props.error.message } </p>
        }

        return(
            <div className={classes.overview}>
                { details }
                { errorMessage}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.album.loading,
        albumName: state.album.albumName,
        albumId: state.album.albumId,
        image: state.album.image,
        href: state.album.href,
        followers: state.album.followers
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getdetails: () => dispatch( albumDetail() )
    }
}

export default connect( mapStateToProps, mapDispatchToProps)(Overview);