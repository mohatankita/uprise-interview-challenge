import React, { Component } from 'react';
import classes from './playlist.module.css';
import { connect } from 'react-redux';
import Loader from '../UI/loader/loader';
import { getPlaylist } from '../../store/actions/dispatcher';
import Slideshow from '../UI/slider/slider';

class Playlist extends Component {

    componentDidMount() {
        this.props.getPlaylistData();
    }

    render() {

        let details = (
            <div className={classes.playlist}>
                <div className="row playlistView">
                    <div className={classes.imageBox}>
                        <Slideshow slideImages={this.props.imagesList}
                                playlistName={this.props.namesList} />
                    </div>
                </div>
            </div>
        );

        if( this.props.loading ) {
            details = <Loader />
        }

        let errorMessage = null;
        if( this.props.error ) {
            errorMessage = <p> { this.props.error.message } </p>
        }

        return(
            <React.Fragment>
                { details }
                { errorMessage}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.album.loading,
        imagesList: state.album.imagesList,
        namesList: state.album.namesList
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getPlaylistData: () => dispatch( getPlaylist() )
    }
}

export default connect( mapStateToProps, mapDispatchToProps )(Playlist);