import React from 'react';
import classes from './loader.module.css';

const loader = (props) => <div className={classes.Loader}>Loading...</div>;

export default loader;