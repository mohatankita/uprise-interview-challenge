import React, { Component } from 'react';
import classes from './dropdown.module.css';

class Dropdown extends Component {
    state = {
        listOpen: false
    }

    componentWillUnmount() {
        this.setState({ listOpen: false})
    }

    toggleList = () => {
        this.setState(prevState => ({ listOpen: !prevState.listOpen }))
    }

    render() {
        return (
            <div className={classes.dropdownWrapper}>
                <div className={classes.dropdownHeader} onClick={() => this.toggleList()}>
                    <div className={classes.dropdownTitle}>{this.props.title}</div>
                </div>
                {this.state.listOpen && <ul className={classes.dropdownList}>
                    {this.props.list.map((item) => (
                        <li className={classes.dropdownListItem} key={item.id} >{item.title}</li>
                    ))}
                </ul>}
            </div>
        )
    }
}

export default Dropdown;