import React from 'react';
import { Slide } from 'react-slideshow-image';
import classes from './slider.module.css';

const properties = {
    transitionDuration: 500,
    infinite: true,
    arrows: true,
    pauseOnHover: true,
}

const Slideshow = (props) => {
    return (
        <div className={classes.slideContainer}>
            <Slide {...properties}>
                {props.slideImages.map((list, index) => {
                    return (
                        <div className={classes.eachSlide}>
                            <div style={{ 'backgroundImage': `url(${list})` }}>
                            </div>
                            <div className={classes.nameSlide}>{props.playlistName[index]}</div>
                        </div>
                    )
                })}
            </Slide>
        </div>
    )
}

export default Slideshow;