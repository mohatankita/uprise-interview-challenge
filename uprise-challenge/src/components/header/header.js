import React, { Component } from 'react';
import classes from "./header.module.css";
import { Small } from '@uprise/text';
import { NavLink } from 'react-router-dom';
import { primary } from '@uprise/colors';
import { connect } from 'react-redux';
import { getArtists } from '../../store/actions/dispatcher';
import Dropdown from '../UI/dropdown/dropdown';

const menubar = [{
    id: 0,
    title: 'hello'
}, {
    id: 1,
    title: 'world'
}]

class Header extends Component {

    setActivePath = (path) => {
        switch(path) {
            case '/overview': return 'Overview'
            case '/playlist': return 'Playlist'
            case '/feature': return 'Featured'
            default : return 'Overview'
        }
    }
    
    state = { active: this.setActivePath(window.location.pathname) }
    
    componentDidMount() {
        this.props.getArtists();
    }

    selectedNavigation = (event) => {
        this.setState({ active: event.target.innerText })
    }

    render() {
        return (
            <>
                <nav className={classes.navigationBar}>
                    <Small>
                        <ul> 
                            <li>
                                <NavLink to="/"
                                    style={ this.state.active === 'Overview' ?
                                        {color: primary.purple} :
                                        {color: primary.charcoal }
                                    }
                                    onClick={(event) => this.selectedNavigation(event)}>
                                    Overview
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/playlist"
                                    style={ this.state.active === 'Playlist' ?
                                        {color: primary.purple} :
                                        {color: primary.charcoal }
                                    }
                                    onClick={(event) => this.selectedNavigation(event)}>
                                    Playlist
                                </NavLink>
                            </li>
                            <li>
                                <NavLink to="/feature"
                                    style={ this.state.active === 'Featured' ?
                                        {color: primary.purple} :
                                        {color: primary.charcoal }
                                    }
                                    onClick={(event) => this.selectedNavigation(event)}>
                                        <Dropdown title="Featured"
                                            list={this.props.artists} />
                                </NavLink>
                            </li>
                        </ul>
                    </Small>
                </nav>
            </>
        )
    }
}

const mapStateToProps = state => {
    return {
        artists: state.album.artists
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getArtists: () => dispatch( getArtists())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);