import { SpotifyGraphQLClient } from 'spotify-graphql';

export const albumDetails = (credentials) => {
    return SpotifyGraphQLClient(credentials).query(`{
        user(id:"chilledcow") {
            id
            display_name
            href
            images{
                url
            }
        }
    }`)
}

export const playlistDetails = (credentials) => {
    return SpotifyGraphQLClient(credentials).query(`{
        user(id:"chilledcow") {
            id
            playlists{
                id
                description
                href
                name
                images {
                    url
                }
            }
        }
    }`)
}

export const artistsDetails = (credentials) => {
    return SpotifyGraphQLClient(credentials).query(`{
        user(id:"chilledcow") {
            id
            playlists{
                id
                tracks(limit: 25) {
                    track{
                        artists{
                            name
                        }
                    }
                }
            }
        }
    }`)
}