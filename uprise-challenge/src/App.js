import React, { Component } from 'react';
import './App.css';
import Header from './components/header/header';
import { Switch, Route, withRouter, Redirect } from 'react-router-dom';
import Overview from './components/overview/overview';
import { auth } from './store/actions/auth';
import { connect } from 'react-redux';
import Loader from './components/UI/loader/loader';
import Feature from './components/featured/featured';
import Playlist from './components/playlist/playlist';

class App extends Component {

  componentDidMount() {
    this.props.onAuthentication();
  }

  render() {
    let header = null;
    if(this.props.loading) {
      header = < Loader />
    } else {
      header = this.props.isAuthenticated ? 
        <div className="header">
          <Header />
        </div> : <p> Not authenticated </p>
    }
  
    return (
      <div className="App">
        { header }
        <Switch>
          <Route path="/playlist" exact component={Playlist} />
          <Route path="/feature" exact component={Feature} />
          <Route path="/" component={Overview} />
          <Redirect to="/" />
        </Switch>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    loading: state.auth.loading
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAuthentication: () => dispatch(auth())
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
