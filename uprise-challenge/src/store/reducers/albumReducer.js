import * as actionTypes from '../actions/actionTypes';

const initialState = {
    albumName: null,
    albumId: null,
    image: null,
    href: null,
    namesList : [],
    imagesList: [],
    followers: null,
    loading: false,
    error: null,
    artists: []
}

const albumReducer = ( state = initialState, action ) => {
    switch(action.type) {
        case actionTypes.FETCH_ALBUM_DATA_INIT:
            return { ...state, loading: true }
        case actionTypes.FETCH_ALBUM_DATA_SUCCESS :
            return {
                ...state,
                albumName: action.albumName,
                albumId: action.albumId,
                image: action.image,
                href: action.href
            }
        case actionTypes.FETCH_ALBUM_DATA_FAIL:
            return { ...state, error: action.error, loading: false }
        case actionTypes.FETCH_ALBUM_FOLLOWERS_INIT:
            return { ...state, loading: true }
        case actionTypes.FETCH_ALBUM_FOLLOWERS_SUCCESS :
            return { ...state, loading: false, followers: action.followers }
        case actionTypes.FETCH_ALBUM_FOLLOWERS_FAIL:
            return { ...state, loading: false, error: action.error }
        case actionTypes.FETCH_ALBUM_PLAYLIST_INIT:
            return { ...state, loading: true }
        case actionTypes.FETCH_ALBUM_PLAYLIST_SUCCESS:
            return {
                ...state, 
                imagesList: action.imagesList,
                namesList: action.namesList,
                loading: false
            }
        case actionTypes.FETCH_ALBUM_PLAYLIST_FAIL:
            return { ...state, loading: false, error: action.error }
        case actionTypes.FETCH_ALBUM_ARTISTS_INIT:
            return { ...state, loading: true }
        case actionTypes.FETCH_ALBUM_ARTISTS_SUCCESS:
            return {
                ...state,
                artists: action.artists,
                loading: false
            }
        case actionTypes.FETCH_ALBUM_ARTISTS_FAIL:
                return { ...state, loading: false, error: action.error }
        default: return { ...state }
    }
}

export default albumReducer;