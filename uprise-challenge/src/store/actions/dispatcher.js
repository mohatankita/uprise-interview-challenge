import * as actionTypes from './actionTypes';
import {albumDetails, playlistDetails, artistsDetails} from '../../services/spotify.api';
import config from '../../services/config.json';
import Axios from 'axios';

const configuration = {
    ...config,
    accessToken: localStorage.getItem('access-token')
}

export const fetchAlbumDataInit = () => {
    return {
        type: actionTypes.FETCH_ALBUM_DATA_INIT
    }
}

export const fetchAlbumDataSuccess = (data) => {
    return {
        type: actionTypes.FETCH_ALBUM_DATA_SUCCESS,
        albumName: data.display_name,
        albumId: data.id,
        image: data.images[0].url,
        href: data.href
    }
}

export const fetchAlbumDataFail = ( error ) => {
    return {
        type: actionTypes.FETCH_ALBUM_DATA_FAIL,
        error: error
    }
}

export const fetchFollowersDataInit = ( ) => {
    return {
        type: actionTypes.FETCH_ALBUM_FOLLOWERS_INIT
    }
}

export const fetchFollowersDataSuccess = (data) => {
    return {
        type: actionTypes.FETCH_ALBUM_FOLLOWERS_SUCCESS,
        followers: data
    }
}

export const fetchFollowersDataFail = ( error ) => {
    return {
        type: actionTypes.FETCH_ALBUM_FOLLOWERS_FAIL,
        error: error
    }
}

export const fetchPlaylistDataInit = ( ) => {
    return {
        type: actionTypes.FETCH_ALBUM_PLAYLIST_INIT
    }
}

export const fetchPlaylistDataSuccess = ( imagesList, namesList ) => {
    return {
        type: actionTypes.FETCH_ALBUM_PLAYLIST_SUCCESS,
        imagesList: imagesList,
        namesList: namesList
    }
}

export const fetchPlaylistDataFail = ( error ) => {
    return {
        type: actionTypes.FETCH_ALBUM_PLAYLIST_FAIL,
        error: error
    }
}

export const fetchArtistsDataInit = ( ) => {
    return {
        type: actionTypes.FETCH_ALBUM_ARTISTS_INIT
    }
}

export const fetchArtistDataSuccess = ( artists ) => {
    return {
        type: actionTypes.FETCH_ALBUM_ARTISTS_SUCCESS,
        artists: artists
    }
}

export const fetchArtistDataFail = ( error ) => {
    return {
        type: actionTypes.FETCH_ALBUM_ARTISTS_FAIL,
        error: error
    }
}

export const albumDetail = () => {
    return dispatch => {
        dispatch(fetchAlbumDataInit());
        albumDetails(configuration)
            .then(response => {
                dispatch(fetchAlbumDataSuccess(response.data.user));
                dispatch(getFollowers(response.data.user.href));
            })
            .catch(error => fetchAlbumDataFail(error.message))
    }
}

export const getFollowers = (followersUrl) => {
    return dispatch => {
        dispatch(fetchFollowersDataInit());
        Axios.get(followersUrl)
             .then(res => dispatch(fetchFollowersDataSuccess(res.data.followers.total)))
             .catch(error => fetchFollowersDataFail(error.message))
    }
}

export const getPlaylist = () => {
    return dispatch => {
        dispatch(fetchPlaylistDataInit());
        playlistDetails(configuration)
            .then(res => {
                let imagesList = [], namesList = [];
                res.data.user.playlists.map(list => {
                    imagesList.push(list.images[0].url);
                    namesList.push(list.name);
                    return 0;
                });
                return dispatch(fetchPlaylistDataSuccess(imagesList, namesList))
            })
            .catch(error => fetchPlaylistDataFail(error.message))
    }
}

export const getArtists = () => {
    return dispatch => {
        dispatch(fetchArtistsDataInit());
        artistsDetails(configuration)
            .then(res => {
                let artistsList = [];
                artistsList = res.data.user.playlists[Math.floor(Math.random() * 3)].tracks.map(list => {
                    return {
                        id: list.track.artists[0].name,
                        title: list.track.artists[0].name
                    }
                });
                const shuffled = artistsList.sort(() => 0.5 - Math.random());
                artistsList = shuffled.slice(0, 10);
                return dispatch(fetchArtistDataSuccess(artistsList))
            })
            .catch(error => fetchArtistDataFail(error.message))
    }
}