import * as actionTypes from './actionTypes';
import axios from 'axios';
import config from '../../services/config.json';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    }
}

export const authSuccess = ( isAuth, accessToken ) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        isAuthenticated: isAuth,
        accessToken: accessToken
    }
}

export const authFail = ( error ) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    }
}

export const auth = () => {
    return dispatch => {
        dispatch(authStart());
        const token = btoa(config.clientId + ':' + config.clientSecret);
        const data = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Basic ' + token
            },
            body: "grant_type=client_credentials"
        };
        axios.post('https://accounts.spotify.com/api/token', data.body, data)
            .then(response => {
                const expirationDate = new Date( new Date().getTime() + response.data.expiresIn * 1000);
                localStorage.setItem('access-token', response.data.access_token);
                localStorage.setItem('expirationDate', expirationDate);
                dispatch(authSuccess(true, response.data.access_token));
            })
            .catch(error => {
                dispatch(authFail(error.response.data.error))})
    }
}