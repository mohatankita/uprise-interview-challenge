import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import {createStore, compose, applyMiddleware, combineReducers} from 'redux';
import authReducer from './store/reducers/authReducer';
import albumReducer from './store/reducers/albumReducer';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk'

const composeEnhancers = process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || compose;

const rootReducers = combineReducers({
  auth: authReducer,
  album: albumReducer
});

const store = createStore(rootReducers, composeEnhancers( applyMiddleware(thunk)));

ReactDOM.render(
  <Provider store={store} >
    <BrowserRouter>
        <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
